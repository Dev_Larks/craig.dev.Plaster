# Plaster Template

## Description
Created to provide scaffolding framework for new PowerShell modules with the current folder structure that I use.

## Author
Authored by Dev_Larks

## Overview
This template scaffolds the folder/file structure for a new module, it prompts for a module name, and the files/directories to be created before creating the desired module structure. Examples of its use are below:

## Installation
Manual
The downloaded module template can be placed in any directory you choose, it is explicitly referenced when creating a new PowerShell module.

## Examples
Invoke-Plaster -TemplatePath 'C:\Users\larkinc\OneDrive - DPIE\Dev\craig.dev.Plaster\FullModuleTemplate' -DestinationPath C:\Data\
  ____  _           _
 |  _ \| | __ _ ___| |_ ___ _ __
 | |_) | |/ _` / __| __/ _ \ '__|
 |  __/| | (_| \__ \ ||  __/ |
 |_|   |_|\__,_|___/\__\___|_|
                                            v1.1.3
==================================================
Enter the name of the module: craig.dev.DPE_WriteResponse
Brief description of this module: Module to manage structured replys to common tasks managed through CSConnect
Enter the version number of the module (0.0.1):
Module author's name: Craig Larkin
Select an editor for editor integration (or None):
[N] None  [C] Visual Studio Code  [?] Help (default is "C"):
Please choose function folders to include
[P] Public
[I] Internal(Private)
[C] Classes
[B] Binaries
[D] Data
[?] Help
(default choices are P,I,C)
Choice[0]:
Include Pester Tests?
[Y] Yes  [N] No  [?] Help (default is "Y"):
Destination path: C:\Data\
Setting up your project
   Create craig.dev.DPE_WriteResponse\craig.dev.DPE_WriteResponse\craig.dev.DPE_WriteResponse.psd1
   Create craig.dev.DPE_WriteResponse\craig.dev.DPE_WriteResponse\craig.dev.DPE_WriteResponse.psm1
 Creating your folders for module: craig.dev.DPE_WriteResponse
   Create craig.dev.DPE_WriteResponse\craig.dev.DPE_WriteResponse\Docs\Images\
   Create Output\
   Create craig.dev.DPE_WriteResponse\craig.dev.DPE_WriteResponse\Public\
   Create craig.dev.DPE_WriteResponse\craig.dev.DPE_WriteResponse\Private\
   Create craig.dev.DPE_WriteResponse\craig.dev.DPE_WriteResponse\Classes\
Creating a Tests folder
   Create craig.dev.DPE_WriteResponse\craig.dev.DPE_WriteResponse\Tests\
   Create craig.dev.DPE_WriteResponse\craig.dev.DPE_WriteResponse\Tests\craig.dev.DPE_WriteResponse.tests.ps1
Creating a VSCode settings and tasks.json files for this module
   Create craig.dev.DPE_WriteResponse\craig.dev.DPE_WriteResponse\vscode\settings.json
   Create craig.dev.DPE_WriteResponse\craig.dev.DPE_WriteResponse\vscode\tasks.json
Deploying Common Files
   Create craig.dev.DPE_WriteResponse\.gitignore
   Create craig.dev.DPE_WriteResponse\ReadMe.md
   Create craig.dev.DPE_WriteResponse\Output\ChangeLog.md
   Create craig.dev.DPE_WriteResponse\craig.dev.DPE_WriteResponse.Build.ps1
  Missing The required module Pester (minimum version: 4.0.3) was not found.
          Without Pester, you will not be able to run the provided Pester test to validate your module manifest file.
          Without version 4.0.3, VS Code will not display Pester warnings and errors in the Problems panel.

Your new PowerShell module project 'craig.dev.DPE_WriteResponse' has been created.

A Pester test has been created to validate the module's manifest file.  Add additional tests to the test directory.
You can run the Pester tests in your project by executing the 'test' task.  Press Ctrl+P, then type 'task test'.